package com.puc.tcc.logistica.historicoPedido.dados;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.puc.tcc.logistica.historicoPedido.dominio.Historico;



public interface IHistoricoDados extends CrudRepository<Historico, Long> {
	
	List<Historico> findByIdPedido(long idPedido);
	
}