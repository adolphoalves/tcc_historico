package com.puc.tcc.logistica.historicoPedido.servicos;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.puc.tcc.logistica.historicoPedido.dados.IHistoricoDados;
import com.puc.tcc.logistica.historicoPedido.dominio.Historico;
import com.puc.tcc.logistica.historicoPedido.dominio.enums.StatusEnum;

@Service
public class ServicosDeHistoricoPedido {
	
	@Autowired
	private IHistoricoDados historicoDados;
	
	public List<Historico> getPorPedido(long idPedido) {
		return historicoDados.findByIdPedido(idPedido);
	}
	
	public void registraHistorico(Historico historico) {
		try {
			historico.setDataOcorrencia(new Date());;
			historicoDados.save(historico);
		} catch (Exception e) {
			//TODO ERROR LAYER
		}
	}
	
	@KafkaListener(topics = "novoPedidoHistorico", id = "novoPedidoHistorico")
	public void mensagemNovoPedido(ConsumerRecord<?, ?> consumerRecord) {
		try {
			Historico hist = montaHistorico(consumerRecord);
			registraHistorico(hist);
		} catch (Exception e) {
			//TODO ERROR LAYER
		}
	}
	
	@KafkaListener(topics = "atualizacaoPedido", id = "atualizacaoPedidoHistorico")
	public void mensagemPedidoTransferido(ConsumerRecord<?, ?> consumerRecord) {
		try {
		    Historico hist = montaHistorico(consumerRecord);
		    registraHistorico(hist);
		} catch (Exception e) {
			//TODO ERROR LAYER
		}
	}
	
	public Historico montaHistorico(ConsumerRecord<?, ?> consumerRecord) throws JsonParseException, JsonMappingException, IOException {
	    String mensagem = (String) consumerRecord.value();
	    ObjectNode node = new ObjectMapper().readValue(mensagem, ObjectNode.class);
	    Historico hist = new Historico();
	    hist.setIdCliente(node.get("idCliente").asLong());
	    hist.setIdPedido(node.get("id").asLong());
	    hist.setDataPedido(new Date(node.get("dataPedido").asLong()));
	    hist.setDataOcorrencia(new Date(consumerRecord.timestamp()));
	    StatusEnum status = StatusEnum.valueOf(node.get("status").asText());
	    hist.setStatusPedido(status);
	    return hist;
	}
	
}
