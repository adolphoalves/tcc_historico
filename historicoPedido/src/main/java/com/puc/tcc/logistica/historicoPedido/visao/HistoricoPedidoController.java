package com.puc.tcc.logistica.historicoPedido.visao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.puc.tcc.logistica.historicoPedido.dominio.Historico;
import com.puc.tcc.logistica.historicoPedido.servicos.ServicosDeHistoricoPedido;
 
@RestController
@RequestMapping("/historico")
public class HistoricoPedidoController {

	@Autowired
	private ServicosDeHistoricoPedido servicosDeHistoricoPedido;
	

	@GetMapping("/{idPedido}")
	ResponseEntity<List<Historico>> getPorPedido(@PathVariable Long idPedido) {
		return ResponseEntity.status(HttpStatus.OK).body(servicosDeHistoricoPedido.getPorPedido(idPedido));
	}
	
}
