package com.puc.tcc.logistica.historicoPedido.visao.adivices;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.puc.tcc.logistica.historicoPedido.visao.exceptions.PedidoNotFoundException;

@ControllerAdvice
public class PedidoNotFoundAdivice {

	@ResponseBody
	@ExceptionHandler(PedidoNotFoundException.class)
	@ResponseStatus(HttpStatus.NOT_FOUND)
	String cargaNotFoundHandler(PedidoNotFoundException ex) {
		return ex.getMessage();
	}

}
